import React, { useState } from 'react';
import ReactDOM from 'react-dom';

const App = (props) => {
  const [selected, setSelected] = useState(0);
  const [votes, setVotes] = useState(new Uint32Array(6));

  const changeVote = () => {
    let localVotes = votes.slice(); // returns a new Uint32Array
    localVotes[selected]++;
    setVotes(localVotes);
  };

  const changeState = () => {
    setSelected(Math.floor(Math.random() * 6));
  };

  const getMostVotedAnec = () => {
    let maxVotes = 0;
    let mostVotedAnecIndex = 0;
    for (let i = 0; i < votes.length; i++) {
      if (votes[i] > maxVotes) {
        maxVotes = votes[i];
        mostVotedAnecIndex = i;
      }
    }
    return anecdotes[mostVotedAnecIndex];
  }

  return (
    <div>
      <div>
        <h2>Anecdote of the day</h2>
        <p>{props.anecdotes[selected]}</p>
        <p>Has {votes[selected]} votes.</p>
        <button onClick={changeVote}>Vote</button>
        <button onClick={changeState}>Next anecdote</button>
      </div>
      <div>
        <h2>Anecdote with most votes</h2>
        <p>{getMostVotedAnec()}</p>
      </div>
    </div>
  );
};

const anecdotes = [
  'If it hurts, do it more often',
  'Adding manpower to a late software project makes it later!',
  'The first 90 percent of the code accounts for the first 90 percent of the development time...The remaining 10 percent of the code accounts for the other 90 percent of the development time.',
  'Any fool can write code that a computer can understand. Good programmers write code that humans can understand.',
  'Premature optimization is the root of all evil.',
  'Debugging is twice as hard as writing the code in the first place. Therefore, if you write the code as cleverly as possible, you are, by definition, not smart enough to debug it.'
];

ReactDOM.render(<App anecdotes={anecdotes} />, document.getElementById('root'));
